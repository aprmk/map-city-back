import { IsNumber, IsString, IsUUID } from 'class-validator';
import { User } from '../../user/user.decorator';
import { ApiModelProperty } from '@nestjs/swagger/dist/decorators/api-model-property.decorator';
import { Remark } from '../../model/remark.entity';

export class RemarkDTO implements Readonly<RemarkDTO> {
  @ApiModelProperty({ required: true })
  @IsNumber()
  //@IsUUID
  id: number;

  @ApiModelProperty({ required: true })
  @IsString()
  text: string;

  public static from(dto: Partial<RemarkDTO>) {
    const it = new RemarkDTO();
    it.id = dto.id;
    it.text = dto.text;
    return it;
  }

  public static fromEntity(entity: Remark) {
    return this.from({
      id: entity.id,
      text: entity.text,
    });
  }

  public toEntity(user: User = null) {
    const it = new Remark();
    it.id = this.id;
    it.text = this.text;
    it.createDateTime = new Date();
    return it;
  }
}
