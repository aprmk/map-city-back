// import * as _ from 'lodash';
// import { createConnection, ConnectionOptions } from 'typeorm';
// import { configService } from '../config/config.service';
// import { User } from '../user/user.decorator';
// import { Category } from '../model/category.entity';
// import { CategoryService } from '../category/category.service';
// import { CategoryDTO } from '../category/dto/category.dto';
// import * as categories from './data/categories.json';
//
// async function run() {
//   const seedUser: User = { id: 'seed-user' };
//
//   const seedId = Date.now()
//     .toString()
//     .split('')
//     .reverse()
//     .reduce((s, it, x) => (x > 3 ? s : (s += it)), '');
//
//   const opt = {
//     ...configService.getTypeOrmConfig(),
//     debug: true,
//   };
//
//   const connection = await createConnection(opt as ConnectionOptions);
//   const categoryService = new CategoryService(
//     connection.getRepository(Category),
//   );
//
//   const work = categories
//     .map((category) => CategoryDTO.from(category))
//     .map((dto) =>
//       categoryService
//         .create(dto, seedUser)
//         .then((r) => (console.log('done ->', r.name), r)),
//     );
//
//   return await Promise.all(work);
// }
//
// run()
//   .then((_) => console.log('...wait for script to exit'))
//   .catch((error) => console.error('seed error', error));
