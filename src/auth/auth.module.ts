import { Module } from '@nestjs/common';
import { PersonModule } from '../person/person.module';
import { PassportModule } from '@nestjs/passport';
import { JwtModule } from '@nestjs/jwt';
import { AuthService } from './auth.service';
import { AuthController } from './auth.controller';
import { JwtStrategy } from './jwt.strategy';

@Module({
  imports: [
    PersonModule,
    PassportModule.register({
      defaultStrategy: 'jwt',
      property: 'person',
      session: false,
    }),
    JwtModule.register({
      secret: process.env.SECRETKEY,
      signOptions: {
        // time in minutes
        expiresIn: process.env.EXPIRESIN + 'm',
      },
    }),
  ],
  controllers: [AuthController],
  providers: [AuthService, JwtStrategy],
  exports: [AuthService],
})
export class AuthModule {}
