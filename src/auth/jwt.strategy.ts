import { PassportStrategy } from '@nestjs/passport';
import { ExtractJwt, Strategy } from 'passport-jwt';
import { HttpException, HttpStatus, Injectable } from '@nestjs/common';
import { PersonDTO } from '../person/dto/person.dto';
import { JwtPayload } from './JwtPayload';
import { AuthService } from './auth.service';

@Injectable()
export class JwtStrategy extends PassportStrategy(Strategy) {
  constructor(private readonly authService: AuthService) {
    super({
      jwtFromRequest: ExtractJwt.fromAuthHeaderAsBearerToken(),
      secretOrKey: process.env.SECRETKEY,
    });
  }

  async validate(payload: JwtPayload): Promise<PersonDTO> {
    const person = await this.authService.validateUser(payload);
    if (!person) {
      throw new HttpException('Invalid token', HttpStatus.UNAUTHORIZED);
    }
    return person;
  }
}
