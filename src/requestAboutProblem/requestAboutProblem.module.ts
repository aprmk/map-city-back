import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { RequestAboutProblem } from '../model/request.entity';
import { RequestAboutProblemService } from './requestAboutProblem.service';
import { RequestAboutProblemController } from './requestAboutProblem.controller';
import { Category } from '../model/category.entity';
import { Person } from '../model/person.entity';
import { File } from '../files/files.entity';

@Module({
  imports: [
    TypeOrmModule.forFeature([RequestAboutProblem, Category, Person, File]),
  ],
  providers: [RequestAboutProblemService],
  controllers: [RequestAboutProblemController],
  exports: [],
})
export class RequestAboutProblemModule {}
