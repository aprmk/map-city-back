import { Injectable, NotFoundException } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { User } from '../user/user.decorator';
import { RequestAboutProblem } from '../model/request.entity';
import { Category } from '../model/category.entity';
import { CreateRequestAboutProblemDto } from './dto/create-requestAboutProblem.dto';
import { RequestAboutProblemDto } from './dto/requestAboutProblem.dto';
import { CategoryDTO } from '../category/dto/category.dto';
import { Person } from '../model/person.entity';
import { Response } from 'express';
import fs from 'fs';
import { File } from '../files/files.entity';
import { IFile } from '../remark/remark';
import { UpdateProblemVisibility } from './dto/update-problem-visibility';

@Injectable()
export class RequestAboutProblemService {
  constructor(
    @InjectRepository(RequestAboutProblem)
    private readonly requestAboutProblemRepository: Repository<RequestAboutProblem>,
    @InjectRepository(Category)
    private categoryRepository: Repository<Category>,
    @InjectRepository(Person)
    private personRepository: Repository<Person>,
    @InjectRepository(File)
    private fileRepository: Repository<File>,
  ) {}

  // public async getAll(): Promise<RequestAboutProblemDto[]> {
  //   return await this.requestAboutProblemRepository
  //     .find()
  //     .then((requestAboutProblems) =>
  //       requestAboutProblems.map((e) => RequestAboutProblemDto.fromEntity(e)),
  //     );
  // }
  ///GET

  async getRequestAboutProblem(): Promise<RequestAboutProblem[]> {
    return this.requestAboutProblemRepository.find({
      relations: ['person', 'category'],
    });
  }

  async getRequestAboutProblemsById(id: number): Promise<RequestAboutProblem> {
    const result = await this.requestAboutProblemRepository.findOne(id, {
      relations: ['person', 'category'],
    });

    if (!result) {
      throw new NotFoundException(
        `RequestAboutProblems with ID: ${id} not found`,
      );
    }

    return result;
  }

  async getRequestAboutProblemImage(
    imgName: string,
    res: Response,
  ): Promise<void> {
    try {
      await fs.promises.stat(`src/uploads/images/${imgName}`);
      return res.sendFile(imgName, { root: 'src/uploads/images' });
    } catch (error) {
      throw new NotFoundException(`Зображення з назвою ${imgName} не знайдено`);
    }
  }

  // GET CATEGORY BY ID
  async getRequestAboutProblemsByCategoryId(
    categoryId: number,
  ): Promise<RequestAboutProblem[]> {
    return await this.requestAboutProblemRepository.find({
      where: { category: categoryId },
      relations: ['category'],
    });
  }

  //POST
  async createRequestAboutProblem(
    dto: CreateRequestAboutProblemDto,
  ): Promise<RequestAboutProblem> {
    const { categoryId, personId, ...otherFields } = dto;
    const relatedCategory = await this.categoryRepository.findOne({
      where: { id: categoryId },
    });

    if (!relatedCategory) {
      throw new NotFoundException(`Категорія з назвою ${categoryId} не існує`);
    }
    const relatedPerson = await this.personRepository.findOne({
      where: { id: personId },
    });

    if (!relatedPerson) {
      throw new NotFoundException(`Людина з id ${personId} не існує`);
    }

    return this.requestAboutProblemRepository.save({
      ...otherFields,
      category: relatedCategory,
      person: relatedPerson,
    });
  }

  // PUT

  async updateProblemVisibility(
    id: number,
    dto: UpdateProblemVisibility,
  ): Promise<RequestAboutProblem> {
    const requestAboutProblem = await this.requestAboutProblemRepository.findOne(
      id,
    );

    if (!requestAboutProblem) {
      throw new NotFoundException(
        `requestAboutProblem width id: ${id} not found`,
      );
    }

    requestAboutProblem.repairsMade = dto.repairsMade;
    await this.requestAboutProblemRepository.update(requestAboutProblem.id, {
      ...requestAboutProblem,
    });

    return requestAboutProblem;
  }

  //DELETE
  async deleteRequestAboutProblems(id: number) {
    const requestAboutProblem = await this.requestAboutProblemRepository.findOne(
      id,
    );

    if (!requestAboutProblem) {
      throw new NotFoundException(
        `RequestAboutProblem with ID: ${id} not found`,
      );
    }

    await this.requestAboutProblemRepository.delete(id);

    return { message: `RequestAboutProblems with ID: ${id} has been deleted` };
  }

  async deleteRequestAboutProblemImageFile(imgName: string) {
    try {
      const query = this.fileRepository.createQueryBuilder('files');
      await query.delete().where('name = :name', { name: imgName }).execute();

      await fs.promises.unlink(`src/uploads/images/${imgName}`);

      return { message: `Зображення: ${imgName} успішно видалено` };
    } catch (error) {
      throw new NotFoundException(
        `Зображення з назвою: ${imgName} не знайдено`,
      );
    }
  }

  async uploadRequestAboutProblemImage(file: IFile): Promise<File> {
    return this.fileRepository.save({
      name: file.filename,
      url: `/src/uploads/images/${file.filename}`,
    });
  }

  public async create(
    dto: RequestAboutProblemDto,
    user: User,
  ): Promise<RequestAboutProblemDto> {
    let category:
      | CategoryDTO
      | Category = await this.categoryRepository.findOne({
      name: dto.category.name,
    });
    if (!category) {
      category = await this.categoryRepository
        .save(dto.category)
        .then((cat) => CategoryDTO.fromEntity(cat));
    }

    dto.categoryId = category.id;

    return this.requestAboutProblemRepository
      .save(dto.toEntity(user))
      .then((e) => RequestAboutProblemDto.fromEntity(e));
  }
}
