import { Entity, Column, OneToMany, ManyToOne } from 'typeorm';
import { BaseEntity } from './base.entity';
import { Person } from './person.entity';
import { RequestAboutProblem } from './request.entity';

@Entity({ name: 'remarks' })
export class Remark extends BaseEntity {
  @Column({ type: 'varchar', length: 300 })
  text: string;

  @ManyToOne((type) => Person, (person) => person.remarks)
  public person: Person;

  @ManyToOne(
    (type) => RequestAboutProblem,
    (requestAboutProblem) => requestAboutProblem.remarks,
  )
  public requestAboutProblem: RequestAboutProblem;
}
