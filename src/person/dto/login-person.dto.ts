import { IsString, IsUUID } from 'class-validator';
import { ApiModelProperty } from '@nestjs/swagger/dist/decorators/api-model-property.decorator';
import { Person } from '../../model/person.entity';

export class LoginPersonDto implements Readonly<LoginPersonDto> {
  @ApiModelProperty({ required: true })
  @IsString()
  login: string;

  @ApiModelProperty({ required: true })
  @IsString()
  password: string;

  // public static from(dto: Partial<PersonDTO>) {
  //   const it = new PersonDTO();
  //   it.id = dto.id;
  //   it.login = dto.login;
  //   it.email = dto.email;
  //   return it;
  // }
  //
  // public static fromEntity(entity: Person) {
  //   return this.from({
  //     id: entity.id,
  //     login: entity.login,
  //     email: entity.email,
  //   });
  // }

  // public toEntity(user: User = null) {
  //   const it = new Person();
  //   it.id = this.id;
  //   it.login = this.login;
  //   it.email = this.email;
  //   it.createDateTime = new Date();
  //   return it;
  // }
}
