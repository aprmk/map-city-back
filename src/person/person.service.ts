import {
  HttpException,
  HttpStatus,
  Injectable,
  NotFoundException,
} from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { User } from '../user/user.decorator';
import { PersonDTO } from './dto/person.dto';
import { Person } from '../model/person.entity';
import { RequestAboutProblem } from '../model/request.entity';
import { CreateRemarkDto } from '../remark/dto/create-remark.dto';
import { Remark } from '../model/remark.entity';
import { CreatePersonDto } from './dto/create-person.dto';
import { RequestAboutProblemDto } from '../requestAboutProblem/dto/requestAboutProblem.dto';
import { CategoryDTO } from '../category/dto/category.dto';
import { Category } from '../model/category.entity';
import { LoginPersonDto } from './dto/login-person.dto';

@Injectable()
export class PersonService {
  constructor(
    @InjectRepository(Person)
    private readonly personRepository: Repository<Person>,
  ) {}

  async findOne(options?: object): Promise<PersonDTO> {
    const user: Person = await this.personRepository.findOne(options);
    return PersonDTO.fromEntity(user);
  }

  async getPersons(): Promise<Person[]> {
    return this.personRepository.find();
  }

  async getPeopleById(id: number): Promise<Person> {
    const result = await this.personRepository.findOne(id);

    if (!result) {
      throw new NotFoundException(`Person with ID: ${id} not found`);
    }

    return result;
  }

  public async createPerson(dto: CreatePersonDto): Promise<PersonDTO> {
    const { login, password, email } = dto;

    const personInDb = await this.personRepository.findOne({
      where: { login },
    });
    if (personInDb) {
      throw new HttpException('User already exists', HttpStatus.BAD_REQUEST);
    }

    const person: Person = await this.personRepository.create({
      login,
      password,
      email,
    });
    await this.personRepository.save(person);
    return PersonDTO.fromEntity(person);
  }

  async findByLogin({ password, login }: LoginPersonDto): Promise<PersonDTO> {
    const person = await this.personRepository.findOne({ where: { login } });

    if (!person) {
      throw new HttpException('User not found', HttpStatus.UNAUTHORIZED);
    }

    const comparePasswords = async function (password, password2) {
      return Promise.resolve(password == password2);
    };

    // compare passwords
    const areEqual = await comparePasswords(person.password, password);

    if (!areEqual) {
      throw new HttpException('Invalid credentials', HttpStatus.UNAUTHORIZED);
    }

    return PersonDTO.fromEntity(person);
  }

  async findByPayload({ login }: any): Promise<PersonDTO> {
    return await this.findOne({
      where: { login },
    });
  }
}
